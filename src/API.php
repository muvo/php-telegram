<?php namespace muvo\telegram;

use GuzzleHttp\Client;

class API
{
    /**
     * @var Client
     */
    protected $client;

    public function __construct(string $token = null)
    {
        $baseUri = 'https://api.telegram.org/bot{token}/';

        $this->client = new Client([
            'base_uri' => str_replace('{token}', $token, $baseUri),
            'verify' => false,
        ]);

        $this->token = $token;
    }

    /**
     * @param $name
     * @param $arguments
     * @return \stdClass
     */
    public function __call($name, $arguments)
    {
        $method = 'get';
        $options = [];

        if (count($arguments) === 1) {
            if (is_array($arguments[0])) {
                $method = 'post';
                $options['json'] = $arguments[0];
            }
        }

        return \GuzzleHttp\json_decode($this->client->request(strtoupper($method), $name, $options)->getBody())->result;
    }
}
